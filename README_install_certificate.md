# install_certificate
Ansible playbooks for installing SSL Certificate.
 
# Roles
Below are the roles used in this playbook

* install_certificate -- Installs the SSL Certificate using this playbook.

# Getting Started

* Create the Directory structures required for SSL Certificate installation.
* Downloads the SSL Certificate generation tool.
* Installs SSL Certificate.
 
## Running Playbook
ansible-playbooks -i inventories -e __host_group_all=servers install_certificate.yml

### Parameters to be modified
* run_as - User to run the playbook
* artifactory_username == Username to access the repo
* artifactory__password == Password to access the repo
* artifactory__url: "https://artifactory.intranet.asia/artifactory/generic_phkl_datarobot_local/"
* __openssl_rpm: OpenSSL RPM file name.
* __openssl_checksum: OpenSSL RPM file checksum
* __csr_privatekey_password: "qAzWsX098!@#"
* __csr_country_name: Country Name
* __csr_email_address: "itdc.tech@prudential.com.hk"
* __csr_locality_name: Locality (Eg:- "Hong Kong" )
* __csr_state_or_province_name: State of Province Name ( Eg:-b"Hong Kong SAR" )

#### Certificate uninstallation

Run the following playbook to uninstall datarobot

ansible-playbook -i inventories -e __host_group_all=servers certificate-uninstall.yml