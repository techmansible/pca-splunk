## RITS�Cloud Log and Event Management
Ansible playbook for installing Splunk.
## Roles
Below is the role used in this playbook
rits-clem-install -- Performs the prerequisites, sets attributes,installs Splunk and start service in workload sequence

## Running Playbook(Splunk installation)
ansible-playbook -i inventories/hosts -e host_group=splunk-uat-group1,splunk-uat-group2 splunk-install.yml


##Parameters which can be modified

	run_as - User to run the playbook
	splunk_user - app user for splunk
	splunk_group - Splunk group name

## Note:

1. update the IPs of workloads for [splunk-uat-group1] and [splunk-uat-group2] in hosts file 
2. Update the tag values of workloads and groups for vms in azure portal

## Running Playbook(Splunk uninstallation)
ansible-playbook -i inventories/hosts -e host_group=splunk-uat-group1,splunk-uat-group2 splunk-uninstall.yml
